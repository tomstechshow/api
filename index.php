<?php
$secret='DB2EAFQFOV34V4PD';
/*
error_reporting(E_ALL);
$host = 'localhost';
$db   = 'userdb';
$user = 'webuser';
$pass = 'password';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$opt = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO($dsn, $user, $pass, $opt);
$sql = "SELECT AuthSecret FROM system";
$go = $pdo->prepare($sql);
$rawdata = $pdo->query($sql);
$row=$rawdata->fetch();
$secret = $row["AuthSecret"];
*/

include_once __DIR__.'/../src/GoogleAuthenticatorInterface.php';
include_once __DIR__.'/../src/FixedBitNotation.php';
include_once __DIR__.'/../src/GoogleAuthenticator.php';
include_once __DIR__.'/../src/GoogleQrUrl.php';
$g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();

if (isset($_GET["auth"])) { 
	$auth=$_GET["auth"]; 
	if ($g->checkCode($secret, $auth)) {
		$data=0;
		if (isset($_GET["name"])) { 
			$name=$_GET["name"]; 
			$data++;
		}
		if ($data == 1) {
			echo $name;
			echo ' ';
			echo 'Success!';
			#update database etc....
		} else {
			echo 'Data input error';
		}
		http_response_code(200);
	} else {
		http_response_code(401);
	}
} else {
	http_response_code(400);
}
?>