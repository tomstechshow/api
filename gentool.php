<?php
include_once __DIR__.'/../src/FixedBitNotation.php';
include_once __DIR__.'/../src/GoogleAuthenticatorInterface.php';
include_once __DIR__.'/../src/GoogleAuthenticator.php';
include_once __DIR__.'/../src/GoogleQrUrl.php';
$secret = 'DB2EAFQFOV34V4PD';
$code = '000000';
if (isset($_POST["secret"])) { $secret=$_POST["secret"]; }
$g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
echo 'Current Code is: ';
echo $g->getCode($secret);
echo "<br>";
if (isset($_POST["code"])) { $code=$_POST["code"]; }
echo "Check if $code is valid: ";
if ($g->checkCode($secret, $code)) {
    echo "YES <br>";
} else {
    echo "NO <br>";
}
echo "<br>";
?>
<form method=post action=gentool.php>  
  <label for="generate">Code:</label><input type="text" id="code" name="code"><br>
  <label for="generate">Generate New Secret:</label><input type="checkbox" id="generate" name="generate"><br>
  <input type="submit" value="Submit"><br>
<?php
if (isset($_POST["generate"])) {
	$secret = $g->generateSecret();
	echo "New Secret: $secret <br>";
	echo "The QR Code for this secret (to scan with the Google Authenticator App:";
	echo "<a href=";
	echo \Sonata\GoogleAuthenticator\GoogleQrUrl::generate('TTS', $secret, 'TomsTechShow');
	echo " target=_blank >Show QR Code</a>";
}
echo '<input type="hidden" id="secret" name="secret" value="'.$secret.'">';
echo '<br>'.$secret.'<br>';
?>
</form>
